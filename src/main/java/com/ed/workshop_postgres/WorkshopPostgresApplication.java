package com.ed.workshop_postgres;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkshopPostgresApplication {

	public static void main(String[] args) {
		SpringApplication.run(WorkshopPostgresApplication.class, args);
	}

}
