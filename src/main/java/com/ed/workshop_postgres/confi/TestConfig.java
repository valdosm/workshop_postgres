package com.ed.workshop_postgres.confi;
import java.util.Arrays;

import com.ed.workshop_postgres.entidades.User;
import com.ed.workshop_postgres.repositorios.UserRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.Profile;

@Configuration
//@Profile("test")
public class TestConfig implements CommandLineRunner{
    @Autowired
    private UserRespository repository;

    @Override
    public void run(String... args) throws Exception {
        
            User user1 = new User(1,"Pedro" ,"valdosm@gmail.com");
            User user2= new User(2,"Carlos" ,"caca@gmail.com");
            User user3 = new User(3,"Maria" ,"maria@gmail.com");
    
            repository.saveAll(Arrays.asList(user1, user2, user3));
        
    }
    
    }

