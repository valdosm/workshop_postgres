package com.ed.workshop_postgres.servico;

import java.util.List;
import java.util.Optional;

import com.ed.workshop_postgres.entidades.User;
import com.ed.workshop_postgres.repositorios.UserRespository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    private UserRespository respository;
    public List<User> findAll(){
        return respository.findAll();

        }
        //buscar usuario por Id
        public User findById(Integer id){
            Optional <User> user = respository.findById(id);
            return  user.get();

      }
    
}
