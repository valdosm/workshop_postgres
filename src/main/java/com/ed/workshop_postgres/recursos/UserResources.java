package com.ed.workshop_postgres.recursos;
import java.util.List;

import com.ed.workshop_postgres.entidades.User;
import com.ed.workshop_postgres.servico.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users")
public class UserResources {
    @Autowired
    private UserService service;
    @GetMapping
    public ResponseEntity<List<User>> findAll(){
        List<User> list = service.findAll();
        return ResponseEntity.ok().body(list);
   
    }
    //@PathVariable considera como paramentro que vai chegar na URL
    @GetMapping(value = "/{id}")
    public ResponseEntity <User> fidById( @PathVariable Integer id){
        User user = service.findById(id);
        return ResponseEntity.ok().body(user);
    }
    
}
