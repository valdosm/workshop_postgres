package com.ed.workshop_postgres.repositorios;

import com.ed.workshop_postgres.entidades.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRespository  extends JpaRepository<User, Integer>{
    
}
